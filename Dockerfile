FROM node:lts-buster-slim

WORKDIR /usr/src/app
ENV PATH="/usr/src/app:${PATH}"

COPY src/package*.json ./

# If you are building your code for production
RUN npm ci --only=production

COPY src .

CMD [ "cli.js"]