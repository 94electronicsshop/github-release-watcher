"use strict"
const getRepoRelease = require('./releases')

function getDaysOld(days) {
  const date = new Date();
  date.setDate(date.getDate() - days);
  return date.getTime()
}

function createMessage(releases) {
  return releases.map(({repo, name, published_at, html_url}) => {
    return `There is a new release in ${repo}: ${name}. It was published on ${published_at}. For more info visit ${html_url}`
  }).join('\n\n')
}

function createErrorMessage({status, message, repo}) {
  return `The repo "${repo}" says: "${message}" (code: ${status})`
}

async function getNewReleaseData(repos, daysOld=1, includePre=true) {
  const newerThan = getDaysOld(daysOld)
  const promises = repos.map(async r => await getRepoRelease(r, newerThan, includePre))
  let releases = []
  try {
    releases = await Promise.all(promises)
  } catch (e) {
    throw e
  }
  return releases.filter(release => release != null)
}

async function getReleasesMessage (reposList, options) {
  const repos = reposList.split(',')
  let message = ''
  try {
    const releases = await getNewReleaseData(repos, options.timePassed, options.preOff)
    if(options.json) {
      message = JSON.stringify(releases)
    } else {
      message = createMessage(releases)
    }
  } catch (e) {
    message = createErrorMessage(e)
  }
  console.log(message)
  return message
}

module.exports = {
  getReleasesMessage,
  getNewReleaseData,
  getDaysOld,
}