"use strict"
const axios = require('axios').default
const semverParse = require('semver/functions/parse')

class ResponseError extends Error {
  constructor(axiosError, repo) {
    super(axiosError.response.statusText)
    this.status = axiosError.response.status
    this.repo = repo
  }
}

async function getRepoReleases(repo, newerThan, includePre=true) {
  repo = repo.trim()
  let response = null
  try {
    response = await axios.get(`https://api.github.com/repos/${repo}/releases`)
  } catch(e) {
    throw new ResponseError(e, repo)
  }

  if(response.data.length === 0) {
    return null
  }

  const found = response.data.find(({published_at, name}) => {
    if(Date.parse(published_at) <= newerThan) return false;
    
    const semver = semverParse(name.split(' ').pop())
    if(!includePre && semver.prerelease.length) return false;

    return true
  })

  if(found) {
    return {
      ...found,
      repo
    }
  }

  return null
}

module.exports = getRepoReleases
module.exports.ResponseError = ResponseError