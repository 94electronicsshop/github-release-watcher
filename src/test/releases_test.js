const moxios = require('moxios')
const should = require('should')
const getRepoReleases = require('../releases')
const getDaysOld = require('../index').getDaysOld

describe('Get repo releases', function() {
  beforeEach(function () {
    // import and pass your custom axios instance to this method
    moxios.install()
  })

  afterEach(function () {
    // import and pass your custom axios instance to this method
    moxios.uninstall()
  })
  describe('If no recent release exists', function() {
    it('it returns null', async function() {
      moxios.stubRequest(/.*team\/old-release.*/, {
        status: 200,
        response: [{
          name: 'outdated',
          body: 'outdated',
          published_at: '2019-12-24T10:45:22Z',
          html_url: ''
        }]
      })

      const response = await getRepoReleases('team/old-release', getDaysOld(1))
      return should(response).be.null()
    });
  });
  describe('if new release exists', function() {
    it('returns important release data', async function () {
      const now = new Date()
      moxios.stubRequest(/.*team\/new-release.*/, {
        status: 200,
        response: [{
          name: 'outdated',
          body: 'outdated',
          published_at: now.toISOString(),
          html_url: ''
        }]
      })

      const response = await getRepoReleases('team/new-release', getDaysOld(1))
      should(response).be.an.Object()
      response.should.only.have.keys('repo', 'name', 'body', 'published_at', 'html_url')
    })
    it('filters out pre-releases', async function() {
      const now = new Date()
      moxios.stubRequest(/.*team\/new-release.*/, {
        status: 200,
        response: [{
          name: 'Kubernetes v1.25.0-alpha.1',
          body: 'new',
          published_at: now.toISOString(),
          html_url: ''
        },
        {
          name: 'Kubernetes v1.24.0',
          body: 'new',
          published_at: now.toISOString(),
          html_url: ''
        }]
      })

      const response = await getRepoReleases('team/new-release', getDaysOld(1), false)
      should(response).be.an.Object()
      return should(response.name).be.eql('Kubernetes v1.24.0')
    })
  })
  describe('if days off is long enough', function() {
    it('returns an old release', async function () {
      var oldDate = new Date();
      oldDate.setDate(oldDate.getDate() - 3);
      moxios.stubRequest(/.*team\/new-release.*/, {
        status: 200,
        response: [{
          name: 'old-but-ok',
          body: 'old-but-ok',
          published_at: oldDate.toISOString(),
          html_url: ''
        }]
      })

      const response = await getRepoReleases('team/new-release', getDaysOld(4))
      should(response).be.an.Object()
      response.should.only.have.keys('repo', 'name', 'body', 'published_at', 'html_url')
    })
  })
  describe('if api returns an error', function() {
    it('throws a response error', async function() {
      getRepoReleases('nonexistent').should.be.rejectedWith({message: "Not found", status: 404, repo: "nonexistent"})
    })
  })
});
