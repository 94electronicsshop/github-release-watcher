const proxyquire = require('proxyquire')
const should = require('should')

const main = proxyquire('../index', {
  './releases': async function(repo) {
    if(repo.startsWith('hasnew/')) {
      return {
        repo, 
        name: 'New release',
        published_at: 'now',
        html_url: 'here'
      }
    } else {
      return null
    }
  }
})

describe('Main', () => {
  describe('getNewReleaseData', () => {
    it('returns a list by default', async () => {
      const newData = await main.getNewReleaseData([], 1)
      should(newData).be.eql([])
    })
    it('returns a list of new releases', async () => {
      const newData = await main.getNewReleaseData(['hasnew/first', 'old/old', 'hasnew/second'], 1)
      newData.should.have.length(2)
    })
  })
  describe('getReleasesMessage', () => {
    it('returns an empty message if no new release is available', async () => {
      const newData = await main.getReleasesMessage('valami/barmi, ize/bize', {timePassed: 1})
      should(newData).be.eql('')
    })
    it('returns a text with new releases', async () => {
      const newData = await main.getReleasesMessage('hasnew/first, ize/bize', {timePassed: 1})
      should(newData).not.be.eql('')
    })
    it('returns the json if requested', async () => {
      const newData = await main.getReleasesMessage('barmi/valami', {timePassed: 1, json: true})
      should(newData).be.eql('[]')
    })
  })
})